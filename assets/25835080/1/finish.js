/* jshint esversion: 6*/

let Finish = pc.createScript('finish');

Finish.attributes.add('X', {type: 'number'});

Finish.attributes.add('Y', {type: 'number'});

// initialize code called once per entity
Finish.prototype.initialize = function() {
    this.entity.collision.on('collisionstart', this.onCollisionStart, this);
    this.Entity1 = this.app.root.findByName('AI_Parent');
    this.Entity2 = this.app.root.findByName('finish');
    this.Entity3 = this.app.root.findByName('Car_Parent');
    this.checker = false;
    
    //this.x = 5;
};

Finish.prototype.onCollisionStart = function (result) {
    
    let x = this.X;
    let y = this.Y;
    
    //console.log(this.x);
    
    let win = this.app.root.findByName('Win');
    let lose = this.app.root.findByName('Lose');
    
    if (result.other == this.Entity1 && this.checker === false) {
        this.checker = false;
        
        this.Entity1.rigidbody.linearVelocity = pc.Vec3.ZERO;
        this.Entity2.rigidbody.linearVelocity = pc.Vec3.ZERO;
        
        this.Entity1.rigidbody.angularVelocity = pc.Vec3.ZERO;
        this.Entity2.rigidbody.angularVelocity = pc.Vec3.ZERO;
        
        lose.enabled = true;
        this.switchOffFunc();
        this.checker = true;
    }
    
    else if (result.other == this.Entity3 && this.checker === false) {
        this.checker = false;
        win.enabled = true;
        this.switchOffFunc();
        this.checker = true;
        
    }

};

Finish.prototype.switchOffFunc = function(dt) {
    this.app.root.findByName("Group").enabled = false;
    this.app.root.findByName("7000").enabled = false;
    this.app.root.findByName("Gas").enabled = false;
};

// swap method called for script hot-reloading
// inherit your script state here
// Finish.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/