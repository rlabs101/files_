var NotSpecified = pc.createScript('notSpecified');

// initialize code called once per entity
NotSpecified.prototype.initialize = function() {
    
};

// update code called every frame
NotSpecified.prototype.update = function(dt) {
    
};

// swap method called for script hot-reloading
// inherit your script state here
// NotSpecified.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/